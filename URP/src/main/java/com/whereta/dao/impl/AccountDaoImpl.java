package com.whereta.dao.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.whereta.dao.IAccountDao;
import com.whereta.mapper.AccountMapper;
import com.whereta.model.Account;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent
 * @time 2015/8/27 16:23
 */

@Repository("accountDao")
public class AccountDaoImpl implements IAccountDao {
    @Resource
    private AccountMapper accountMapper;

    @Override
    public Account get(int id) {
        return accountMapper.selectByPrimaryKey(id);
    }

    /**
     * @param account
     * @return
     */
    public Account getByAccount(String account) {
        return accountMapper.getByAccount(account);
    }

    @Override
    public int save(Account account) {
        return accountMapper.insertSelective(account);
    }

    /**
     * 删除账号
     * @param accountId
     * @return
     */
    @Override
    public int delete(int accountId) {
        return accountMapper.deleteByPrimaryKey(accountId);
    }

    /**
     * 账号管理页面获取账号id
     * @return
     */
    @Override
    public PageInfo<Map> selectAccountIdManage(String querySql,int pageNow,int pageSize) {
        PageHelper.startPage(pageNow,pageSize);
        List<Map> maps = accountMapper.selectAccountManage(null,null);
        PageInfo<Map> pageInfo = new PageInfo<Map>(maps);
        return pageInfo;
    }

    /**
     * 用户管理
     * @param depIdSet  要查询的部门id集合
     * @param excludeAccountIdSet   排除的不查询的账号id集合
     * @param pageNow
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<Map> selectAccountManage(Set<Integer> depIdSet, Set<Integer> excludeAccountIdSet, int pageNow, int pageSize) {
        PageHelper.startPage(pageNow,pageSize);
        List<Map> maps = accountMapper.selectAccountManage(depIdSet, excludeAccountIdSet);
        PageInfo<Map> pageInfo = new PageInfo<Map>(maps);
        return pageInfo;
    }
}
