package com.whereta.model;

import java.io.Serializable;

public class Role implements Serializable {
    /**
     * `role`.id (角色id)
     * @ibatorgenerated 2015-09-01 18:31:24
     */
    private Integer id;

    /**
     * `role`.name (角色名字)
     * @ibatorgenerated 2015-09-01 18:31:24
     */
    private String name;

    /**
     * `role`.key (角色key)
     * @ibatorgenerated 2015-09-01 18:31:24
     */
    private String key;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}